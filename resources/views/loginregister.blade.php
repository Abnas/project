@extends('master')

@section('context')
<div class="col-md-4 offset-md-4">
    <a href="login" class="btn btn-primary">Login</a>
    <a href="register" class="btn btn-secondary">Register</a>
</div>
@endsection