@extends('master');

@section('context')

<div class="col-md-12 ">
    <table class="table">
        <thead>
          <tr>
            <th scope="col">Product</th>
            <th scope="col">Price</th>
            <th scope="col">Quantity</th>
            <th scope="col">Total</th>
          </tr>
        </thead>
        <tbody>
          <tr id="row" >
            <th id="name">

                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <label class="input-group-text" for="inputGroupSelect01">Options</label>
                    </div>
                     <select name="product" id="product" class="select item_id">
                       <option class="option">--SELECT--</option>
                        @foreach($datas as $data)
                        <option value="{{$data->id}}">{{$data->name}}</option>
                        @endforeach
                       
                      </select>
                  </div>
            </th>
            <td id="price">
                <input   id="quantity_price" type="number" class="form-control input price"/>
            </td>
            
            <td id="quantity">
                <input   id="quantity_input" type="number" class="form-control input qinput"/>
            </td>
            <td id="totalprice">
      
                <input type="number" id="total" class="form-control input total"/>
            </td>

            <td>
                <a class="btn btn-danger delete" >X</a>
            </td>
          </tr>
          
        </tbody>
        
      </table>

      <a class="btn btn-primary" id="add">+</a>

</div>


<script src="https://code.jquery.com/jquery-3.5.1.js" 
integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>


<script>
 


    $(document).ready(function(){

        // $('#delete').click(function(){
        //     $("#quantity_price").val(null);
        //     $("#quantity_input").val(null);
        //     $("#product").val(null);
        //     $('#total').val(null);

        // });


        $("table").on('change','.item_id',function () {
          
          var selected=$(this,"option:selected").val();
          $(this).each(function() {
          $('option[value="' + selected + '"]').attr('disabled','disabled');
        });
        alert(selected);
      });

      

      

        // $("select.positionTypes").change(function () {
        // $("select.positionTypes option[value='" + $(this).data('index') + "']").prop('disabled', false);
        // $(this).data('index', this.value);
        // $("select.positionTypes option[value='" + this.value + "']:not([value=''])").prop('disabled', true);
        // });

     
        $("table").on('keyup','.input',function(){
            
            //

            let a=$(this).closest("tr");
            console.log(a);
            let price=a.find('.price').val();
            let quantity=a.find('.qinput').val();

            let total=price*quantity;
            
            a.find('.total').val(total);
            
        })
        // $(".input").keyup(function(){
        //   alert("hello");
        //     let price=$('#quantity_price').val();
        //     let quantity=$('#quantity_input').val();

        //     let total=price*quantity;
        //     $('#total').val(total);
        // });



        // let val=0;
        // ++val;
        // $("#add").click(function(){
        //     // $('tbody').append('<th class="row">hello</th>');
            
        //     let abc='<tr id="row'+val+'"><th id="name"><div class="input-group mb-3"><div class="input-group-prepend"><label class="input-group-text" for="inputGroupSelect01">Options</label></div><select name="product" id="product" >@foreach($datas as $data)<option value="{{$data->name}}">{{$data->name}}</option>@endforeach</select></div></th><td id="price"><input   id="quantity_price" type="number" class="form-control input"/></td><td id="quantity"><input   id="quantity_input" type="number" class="form-control input"/></td><td id="totalprice"><input type="number" id="total" class="form-control"/></td><td><a class="btn btn-danger delete" id="delete" >X</a></td></tr>'
        //     $('tbody').append(abc);
           
        // });

        // let val=0;
        // ++val;
        // $("#add").click(function(){
        //     // $('tbody').append('<th class="row">hello</th>');
            
        //     let abc='<tr id="row"><th id="name"><div class="input-group mb-3"><div class="input-group-prepend"><label class="input-group-text" for="inputGroupSelect01">Options</label></div><select name="product" id="product" >@foreach($datas as $data)<option value="{{$data->name}}">{{$data->name}}</option>@endforeach</select></div></th><td id="price"><input   id="quantity_price" type="number" class="form-control input"/></td><td id="quantity"><input   id="quantity_input" type="number" class="form-control input"/></td><td id="totalprice"><input type="number" id="total" class="form-control"/></td><td><a class="btn btn-danger delete" id="delete" >X</a></td></tr>'
        //     $('tbody').append(abc);
           
        // });

        // $('#add').click(function(){
        //   $('#row').select(function(){
        //     let a=$(this).clone();
        //     $('.input').val("");
        //     $(this).append(a);

        //   })



          
        $('#add').click(function(){
            // console.log("hello");
            // let a=$(this).parents("#row").clone();
            // console.log(a);
            // a.find("input[type=number]").val("");
            // a.insertAfter($(this).parents("#row"));
            // a.append(a);
            
            // console.log("hello");
            // let prev=[];
            // let ab=$('.select').find(this,function(){
              
            //   prev=$(this.value); 
            //   return prev;
                          
            // });
            // console.log(prev);

            let a=$('#row').clone();
            a.find("input[type=number]").val("");
            a.appendTo("tbody");

            
           
          
        });

        $("table").on('click','.delete',function(){
            let row=$("tr").length;
            alert(row);
            let a= $(this).closest("tr");
            console.log(a);

            if(row==2 &&  a.is(":first-child")){
              $(".input").val("");
              $(".item_id").val("--SELECT--");
              let a =$(this).closest("tr");
            
              // $(".item_id option:").removeAttr("disabled");
              // a.find(".item_id option").removeAttr("disabled");
              
          
            }
            else{
              var selected=$(this,"option:selected").val();
              $(this).each(function() {
                $('option[value="' + selected + '"]').removeAttr("disabled");
              });
            
              a.remove();
              
            }


            //
            // if(!a.is(":first-child")){
            //   a.remove();
            //   }
            // else{
            //   b=confirm("Are you sure?");
            //   // $("#quantity_input,#quantity_price,#total").val("");
            //   if(b==true){

              
            //   $(".input").val("");
            //   a.remove();
            //   }
             
              
        
            
            
            
        })


        $("#product").on("change",function(){
          let a=$("#product").val();
          alert(a);

          $.ajax({
            method:"get",
            // url:'{!!URL::to('/findproduct')!!}',
            // url:'/findproduct/{data}',
            data:{"id":a},
            success:function(result){
              console.log("abns");
            },
            error:function(){
                alert("Error");
            }
             
          })

        })



    });




    

   
</script>








@endsection