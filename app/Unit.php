<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class Unit extends Model
{
    public function product(){
        return $this->belongTo(Product::class);
    }
}
