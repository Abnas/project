<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Product;

class Productcategory extends Model
{
    protected $table = 'productcategories';

    public function products(){
        return $this->hasMany(Product::class);
    }


}
