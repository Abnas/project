<?php

namespace App;
use App\Productcategory;
use App\Unit;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function productcategory(){
        return $this->belongsTo(Productcategory::class);
    }

    public function unit(){
        return $this->hasOne(Unit::class);
    }
}
